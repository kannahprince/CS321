/**
 * Created by princekannah on 3/21/16.
 * Working project: P3_Hashtable.
 */

public class HashObject<T> implements Comparable<T>{
    private T object;
    private int frequencyCount;
    private long key;

    /**
     * CONSTRUCTOR: Creates a hash object wrapper around the specified object.
     * @param object the object to be wrapped.
     */
    public HashObject(T object){
        this.object = object;
        key = getHash(object);
        frequencyCount = 0;
    }

    /**
     * Update the frequency count of this object by one.
     */
    public void updateFrequency(){frequencyCount++;}

    /**
     * Get the frequency (i.e number of occurrences) of this object.
     * @return the number of occurrences of this object
     */
    public int getFrequency() {
        return frequencyCount;
    }

    /**
     * Get the wrapped object.
     * @return the wrapped object.
     */
    public T getObject() {
        return object;
    }

    /**
     * Returns the hash value of this object.
     * @return the hash value
     */
    public Long getKey(){return key;}

    /**
     * Compute a hash key for this object.
     * @param object the object to compute a hash for.
     * @return the computed hash value
     */
    private long getHash(T object) {
        long value = 0;
        if (object instanceof String)
            value = Math.abs(object.hashCode());
        if (object instanceof Integer | object instanceof Long)
            value = Math.abs((Long) object);
        return value;
    }

    @Override
    public int compareTo(Object o) {
        if (o.equals(object))
            return 0;
        else
            return 1;
    }

    @Override
    public String toString() {
        return  " " + object.toString() + " " + frequencyCount;
    }

}
