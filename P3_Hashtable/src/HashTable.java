/**
 * Created by princekannah on 3/21/16.
 * Working project: P3_Hashtable.
 */
@SuppressWarnings ("unused")
public class HashTable {
    public enum PROBING_TYPE{LINEAR_PROBING, DOUBLE_HASHING}
    public PROBING_TYPE probingType;
    private int tableSize, load;
    public int duplicates, totalNumProbes, totalNumInserted;
    public int probingCount[];
    private HashObject[] table;

    /**
     * CONSTRUCTOR: creates a new Hash table with the specified size, load factor and
     * and inserts based the the probing type.
     * @param size size of the table
     * @param probe the insertion algorithm to use when inserting into the table.
     * @param loadFactor the load factor of the table.
     */
    HashTable(int size, HashTable.PROBING_TYPE probe, double loadFactor) {
        probingType = probe;
        duplicates = totalNumProbes = totalNumInserted = 0;
        this.tableSize = size;
        table = new HashObject[tableSize];
        probingCount = new int[tableSize];

        load = (int) (loadFactor * tableSize);
    }

    /**
     * Insert a <code>HashObject</code> into the table
     * @param object the HashObject to be inserted into the table
     * @param K the hash value of the object
     * @return the index the object was inserted at; otherwise a -1 if overflow.
     */
    public int insert(HashObject object, long K)
    {
        while (totalNumInserted <= load) {
            for (int i = 0; i < tableSize; i++) {
                int tableIndex = hashIndex(K, i);
                if (table[tableIndex] == null) {
                    table[tableIndex] = object;
                    probingCount[tableIndex] = i + 1;
                    totalNumProbes += (i + 1);
                    totalNumInserted++;
                    return tableIndex;
                } else if (table[tableIndex].compareTo(object.getObject()) == 0) {
                    table[tableIndex].updateFrequency();
                    duplicates++;
                    return tableIndex;
                }
            }
        }
        return -1;
    }

    /**
     * Gets the average number of probes for the table.
     * @return the average number of probes
     */
    public double getAverage() {
        return (double) totalNumProbes / (double) totalNumInserted;
    }

    /**
     * Gets the number of probes at insertion.
     * @return an array containing the number of probes at insertion.
     */
    public int[] getProbingCount() {
        return probingCount;
    }

    /**
     * Gets a computed hash index.
     * @param key the key-value of the Object.
     * @param i the number to offset the key by.
     * @return a computed hash index.
     */
    private int hashIndex(long key, int i) {
        if (probingType == PROBING_TYPE.LINEAR_PROBING)
            return ((int) ((key % tableSize) + i) % tableSize);
        else
            return ((int) (((key % tableSize) + i * (1 + (key % (tableSize - 2)))) % tableSize));
    }

    @Override
    public String toString()
    {
        StringBuilder string = new StringBuilder("\n");
        for (int j = 0; j < tableSize; j++){
            if (table[j] != null){
                string.append("table[").append(j).append("]: ");
                string.append(table[j].toString());
                string.append("\n");
            }
        }
        return string.toString();
    }

    /**
     * Print the hash table with the total number probes at time of insertion
     * @return the hash table as string with total number of probes at insertion time.
     */
    public String printDebugTable()
    {
        StringBuilder string = new StringBuilder("\n");
        for (int j = 0; j < tableSize; j++) {
            if (table[j] != null) {
                string.append("table[").append(j).append("]: ");
                string.append(table[j].toString());
                string.append(" ").append(probingCount[j]);
                string.append("\n");
            }
        }
        return string.toString();
    }

    private static void fileWriter(){

    }
}
