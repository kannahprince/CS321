/**
 * Created by princekannah on 3/21/16.
 * Working project: P3_Hashtable.
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.Scanner;

import javax.swing.Timer;

public class HashTest{
    public static final long MIN = 95500,MAX = 96000;
    public static final int ERROR_CODE = 1;
    private static final int RANDOM = 1, SYSTIME = 2, FILE = 3;
    private static double userLoadFactor;
    private static int source, debugLevel = 0;
    private static final int DELAY = 1000; //milliseconds
    private static boolean isDone = false;
    private static Timer timer;

    public static void main(String[] args)
    {

        if (args.length < 1){
            System.err.println(printError());
            System.exit(ERROR_CODE);
        }
//
        getInputs(args);
        int size = (int)PrimeGenerator.primeFinder(MIN,MAX);
        int linearOverFlow = 0, doubleOverFlow = 0;
        HashTable linear = new HashTable(size, HashTable.PROBING_TYPE.LINEAR_PROBING, userLoadFactor);
        HashTable doubleHashing = new HashTable(size, HashTable.PROBING_TYPE.DOUBLE_HASHING, userLoadFactor);
        startAnimation();
        if (source == RANDOM) {
            Random rand = new Random();
            while (linearOverFlow != -1 && doubleOverFlow != -1){
                long randNum = Math.abs(rand.nextLong());
                HashObject<Long> randObject = new HashObject<>(randNum);
                long key = randObject.getKey();

                linearOverFlow = linear.insert(randObject, key);
                doubleOverFlow = doubleHashing.insert(randObject, key);
                isDone = false;
            }
            isDone = true;
            timer.stop();
        } else if (source == SYSTIME) {
            while (linearOverFlow != -1 && doubleOverFlow != -1){
                long currentTime = System.currentTimeMillis();
                HashObject<Long> sysTimeObject = new HashObject<>(currentTime);
                long key = sysTimeObject.getKey();

                linearOverFlow = linear.insert(sysTimeObject, key);
                doubleOverFlow = doubleHashing.insert(sysTimeObject, key);
                isDone = false;
            }
            isDone = true;
            timer.stop();
        } else if (source == FILE){
            try (Scanner scan = new Scanner(new File("./artifacts/sampleIO/word-list"))){
                while (linearOverFlow != -1 && doubleOverFlow != -1){
                    while (scan.hasNextLine()){
                        String word = scan.nextLine();
                        HashObject<String> stringHashObject = new HashObject<>(word);
                        long key = stringHashObject.getKey();

                        linearOverFlow = linear.insert(stringHashObject, key);
                        doubleOverFlow = doubleHashing.insert(stringHashObject, key);
                        isDone = false;
                    }
                }
                isDone = true;
                timer.stop();
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            }
        } else{
            System.err.println("No valid input source provided \ninput type:\n" + "\t1 - random numbers \n" +
                    "\t2 - current system time\n" + "\t3 - file\n");
            System.exit(ERROR_CODE);
        }

        if(debugLevel == 1){
            System.out.println(linear);
        }
        else  if(debugLevel == 2)
            System.out.println(linear.printDebugTable());

        DecimalFormat frmt = new DecimalFormat("#.########");
        String linearOut = "\nUsing linear hashing...." +
                "\nInserted " + linear.totalNumInserted + " with " + linear.duplicates + " duplicates." +
                "\nload factor = " + userLoadFactor + ", Avg. no of probes " + frmt.format(linear.getAverage());

        String doubleOut = "Using double hashing:....\n" + "Inserted " + doubleHashing.totalNumInserted + " with "
                + doubleHashing.duplicates + " duplicates.\n" + "load factor = " + userLoadFactor + ", Avg. no of probes "
                +frmt.format(doubleHashing.getAverage());

        System.out.println(linearOut + "\n");
        System.out.println(doubleOut);

    }

    public static String printError() {
        return "Usage: <input type> <load factor> [<debug level>]\n" +
                "input type:\n\t1 - random numbers \n\t2 - current system time\n\t3 - file\n" +
                "load factor: 0 < A < 1" + "\ndebug level:\n\t0 - print summary (default)\n\t1 - print table" +
                "\n\t2 - print number of probes for each insert";
    }

    public static void getInputs(String[] args){

        try{
            source = Integer.parseInt(args[0]);
            userLoadFactor = Double.parseDouble(args[1]);

            if (args.length == 3 && args[2] != null)
                debugLevel = Integer.parseInt(args[2]);
        } catch (IndexOutOfBoundsException e){
            System.err.println(printError());
            System.exit(ERROR_CODE);
        }
    }

    public static class PrimeGenerator{
        public static boolean isPrime(long num) {
            if (num < 2)
                return false;
            if (num == 2 || num == 3)
                return true;
            if (num % 2 == 0 || num % 3 == 0)
                return false;

            long squareRoot = (long) (Math.sqrt(num) + 1);
            for (long i = 6L; i <= squareRoot; i += 6)
                if (num % (i - 1) == 0 || num % (i + 1) == 0) {
                    return false;
                }
            return true;
        }

        public static long primeFinder(long start, long end) {
            for (; start <= end; start++)
                if (isPrime(start) && isPrime(start - 2))
                    return start;
            return -1;
        }
    }
    
     /**
     * Starts the "progress bar" timer
     */
    private static void startAnimation() {
        TimerActionListener taskPerformer = new TimerActionListener();
        timer = new Timer(DELAY, taskPerformer);
        timer.start();
    }
    
     /**
     * Timer action listener class. Use to provide a "progress bar"
     */
    private static class TimerActionListener implements ActionListener {
        /**
         * The action performed when the timer event is fired
         * @param evt the event
         */
        public void actionPerformed(ActionEvent evt) {
            if (!isDone)
                System.out.print(".");
        }
    }
}