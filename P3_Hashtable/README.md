# Project 3: Hash Table
* Name: Prince Kannah
* Class: CS321

## Overview:
This assignment asks to design a [hash table](https://en.wikipedia.org/wiki/Hash_table). I then test my implementation by writing a program that simulates adding to the table from different input sources and varying load factors. Some statistics are collected and reported at the end of the simulation. See **[this README](artifacts/sampleIO/README)** for the results of previous runs. The assignment's full specification can be found [here](https://drive.google.com/file/d/0B_E6vm7B7O5TRm1Ra0g3TXV1eW8/view).

## Included files:
The included files are shown in the directory tree below:
	
	├── Makefile
    ├── README.md
    ├── artifacts
    │   └── sampleIO
    │       ├── README
    │       ├── sample_result.txt
    │       └── word-list
    └── src
        ├── HashObject.java
        ├── HashTable.java
        └── HashTest.java

## Building & Running:
**_NOTE: All program files should be in the same directory. Also, all commands are issued from the terminal._**

This project has a makefile that can be use so simplify compiling and running the program. To compile and run the program with the default values issue the command `make HashTest`. The following arguments can be specified when issuing the `make HashTest` command:
    
    * INPUT_TYPE=the source of data being input into the table (valid options are 1, 2, or 3)
    * LOAD_FACTOR=how full the table is allowed before it's capacity is increased (valid options are 0...1)
    * DEBUG_LEVEL=the level of detail to print (valid options are 0, 1, or 2)
    
An argument's value can be set along with the `make` command as follow:
    
    make HashTest INPUT_TYPE=2 DEBUG_LEVEL=1 LOAD_FACTOR=0.6