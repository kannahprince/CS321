# Project 1: Caching
* Name: Prince Kannah
* Class: CS321

## Overview:
This assignment asks to design a [cache](http://searchstorage.techtarget.com/definition/cache) implementation using a [Linked List](https://en.wikipedia.org/wiki/Linked_list) data structure. I then test my implementation by writing a program that simulate either a one or two level cache. Some statistics are collected anre reported at the end of the simulaiton. The assignment's full specification can be found [here](https://drive.google.com/file/d/0B_E6vm7B7O5TUGJXNndRdFhCM1U/view) 

## Included files:
The included files are shown in the directory tree below:

	├── Makefile
	├── README.md
	├── artifacts
	│   └── sampleIO
	│       ├── Encyclopedia.txt
	│       ├── name.txt
	│       ├── result1k2k
	│       ├── result1k5k
	│       ├── small.txt
	│       └── smallResults
	└── src
    	├── Cache.java
    	└── Test.java

## Building & Running:
**_NOTE: All program files should be in the same directory. Also, all commands are issued from the terminal._**

This project has a makefile that can be use so simplify compiling and running the program. To compile and run the program with the default values issue the command `make Test`. The following arguments can be specified when issuing the `make Test` command:
    
    * RUN_FILE=path to input file
    * CACHE_LEVEL=system of cache to use (valid options are 1 or 2)
    * CACHE_ONE_SIZE=the size of the first cache
    * CACHE_TWO_SIZE=the size of the second cache (required if CACHE_LEVEL is 2)

An argument's value can be set along with the `make` command as follow:
    
    make Test CACHE_LEVEL=1 CACHE_ONE_SIZE=100 RUN_FILE=../artifacts/sampleIO/Encyclopedia.txt
