import java.util.LinkedList;

/**
 * The <code>Cache</code> class is a simple cache using a Doubly Linked List implementation. A size limit
 * must be specified at initialization.
 *
 * @param <T> object type to store in cache.
 * @author Prince Kannah
 * @version 1.0 (SPRING 16)
 */
public class Cache<T> {
    private LinkedList<T> jCache;
    private int cacheLimit;

    /**
     * CONSTRUCTOR: Create a new <code>Cache</code> object with the specified size.
     *
     * @param sizeLimit the size of the cache (i.e the total number of objects
     *                  to be stored in this cache).
     */
    public Cache(int sizeLimit) {
        cacheLimit = sizeLimit;
        jCache = new LinkedList<>();
    }

    /**
     * Add an object to the Cache.
     *
     * @param object object to add to cache
     */
    public void add(T object) {
        if (jCache.size() < cacheLimit) //check for overflow
        {
            //Check for double and remove it
            if (jCache.contains(object)) {
                jCache.remove(object);
            }
            //then add object to cache
            jCache.addFirst(object);
        } else {
            if (jCache.contains(object)) {
                jCache.remove(object);
                jCache.addFirst(object);
            } else {
                jCache.removeLast();
                jCache.addFirst(object);
            }
        }
    }

    /**
     * Remove the specified object from Cache.
     *
     * @param object the object to remove from Cache.
     */
    public void removeObject(T object) {
        jCache.remove(object);
    }

    /**
     * Checks the <code>Cache</code> for the specified object.
     *
     * @param object the object to check in Cache for.
     * @return <code>true</code> if the Cache contains the object; <code>false</code>
     * otherwise.
     */
    public boolean hit(T object) {
        return jCache.contains(object);
    }

    /**
     * Clear the <code>Cache</code>.
     */
    public void clear() {
        jCache.clear();
    }

    /**
     * Gets the specified object from cache. If the object is found, it is moved to the top of the cache.
     *
     * @param Object the object to get from cache.
     * @return the specified object.
     */
    public T getObject(T Object) {
        T target = jCache.get(jCache.indexOf(Object));
        add(target);
        return target;
    }

    /**
     * Gets the current number of objects in cache.
     *
     * @return the current number of objects in cache.
     */
    public int getSize() {
        return jCache.size();
    }

    /**
     * Get this <code>Cache</code>'s limit specified at initialization.
     *
     * @return the total number of objects this cache can hold.
     */
    public int getCacheLimit() {
        return cacheLimit;
    }

    @Override
    public String toString() {
        return jCache.toString();
    }
}


