# Project 2: CPU Scheduling
* Name: Prince Kannah
* Class: CS321

## Overview:
This assignment asks to design a [priority queue](https://en.wikipedia.org/wiki/Priority_queue) using a [max heap](http://courses.cs.vt.edu/cs2604/spring02/Notes/C07.Heaps.pdf) data structure. I then test my implementation by writing a program that simulates a round robin CPU scheduling algorithm. Some statistics are collected and reported at the end of the simulation. The assignment's full specification can be found [here](https://drive.google.com/file/d/0B_E6vm7B7O5TamtuYllzWEIxOXM/view).

## Included files:
The included files are shown in the directory tree below:
	
	├── Makefile
	├── README.md
	├── artifacts
	│   └── sampleIO
	│       └── sample_result
	└── src
    	├── Averager.java
    	├── CPUScheduling.java
    	├── HeapunderFlowException.java
    	├── KeyMismatchException.java
    	├── MaxHeap.java
    	├── PQueue.java
    	├── Process.java
    	└── ProcessGenerator.java

## Building & Running:
**_NOTE: All program files should be in the same directory. Also, all commands are issued from the terminal._**

This project has a makefile that can be use so simplify compiling and running the program. To compile and run the program with the default values issue the command `make CPUScheduling`. The following arguments can be specified when issuing the `make CPUScheduling` command:
    
    * MAX_PROCESS_TIME= the largest possible time units required to finish a process
    * MAX_PRIORITY_LEVEL=the highest priority level for a process
    * INCREMENT_TIME=the time unit before a process's priority level is incremented by one
    * SIMULATION_TIME=the total time units for the simulation 
    * ARRIVAL_RATE=the rate at which new processes arrive

An argument's value can be set along with the `make` command as follow:
    
    make CPUScheduling ARRIVAL_RATE=0.5 SIMULATION_TIME=100 INCREMENT_TIME=5 MAX_PRIORITY_LEVEL=6