
/**
 * PQueue is a <code>Process</code> priority queue implemented using a max-heap data structure.
 * @author Prince Kannah
 * @version 1.0 (SPRING 16, P2 - CPU Scheduling)
 * @see MaxHeap
 * @see Process
 */
public class PQueue {
    private MaxHeap<Process> queue;

    /**
     * CONSTRUCTOR: Creates a new, empty priority queue.
     */
    public PQueue(){
        queue = new MaxHeap<>();
    }

    /**
     * Removes the highest priority value item from the queue.
     * @return the item with the highest priority value.
     */
    public Process dePQueue(){
        return queue.extractMax();
    }

    /**
     * Update all processes in the queue.
     * @param timeToIncrementPriority the time unit during which a process has not received any work.
     * @param maxLevel the highest priority level allowed.
     */
    public void update(int timeToIncrementPriority, int maxLevel){
        for(int i = MaxHeap.ROOT; i <= queue.size(); i++){
            Process p = queue.get(i);
            p.incrementTNP();
            if(timeToIncrementPriority <= p.getTNP()){
                if(p.getPriority() < maxLevel)
                {
                    p.incrementPriority();
                }
                p.resetTimeNotProcessed();
            }
        }
        //Rebuild the heap in-case any of the new updated processes have move up.
        queue.buildMaxHeap();
    }

    /**
     * Add a Process to the queue.
     * @param p the Process to be added to the queue.
     */
    public void enPQueue(Process p){
        queue.insert(p);
    }

    /**
     * Returns <code>true</code> is the queue is empty.
     * @return <code>true</code> if queue contains no objects; <code>false</code> otherwise.
     */
    public boolean isEmpty(){
        return queue.size() < MaxHeap.ROOT;
    }

    /**
     * Clear the queue of all processes.
     */
    public void clearQueue(){
        queue.clearHeap();
    }

    @Override
    public String toString(){
        return queue.toString();
    }
}
