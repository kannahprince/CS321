/**
 * The KeyMismatchException is use when a key being inserted into a heap exceeds
 * the current value.
 * @author Prince Kannah
 * @version 1.0 (Spring 16, CPU-Scheduling)
 * @see MaxHeap
 */

public class KeyMismatchException extends RuntimeException {

    /**
     * DEFAULT CONSTRUCTOR: Creates a new exception.
     */
    public KeyMismatchException(){
        super();
    };

    /**
     * CONSTRUCTOR: Creates a new exception with the message.
     * @param message message to shown when exception is thrown.
     */
    public KeyMismatchException(String message){super(message);}
}
