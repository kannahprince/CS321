import java.util.Random;

/**
 * ProcessGenerator is use to randomly generate a process within a given
 * probability.
 * 
 * @author Prince Kannah
 * @version 1.0 (Spring 16, CPU-Scheduling)
 * @see Process
 * @see MaxHeap
 * @see PQueue
 */

public class ProcessGenerator {
	private Random rand;
	private double probability;

	/**
	 * CONSTRUCTOR: Create a new <code>ProcessGenerator</code> that generates a
	 * new process within the given probability.
	 * 
	 * @param probability
	 *            the probability within which to generate a new Process.
	 */
	public ProcessGenerator(double probability) {
		this.probability = probability;
		rand = new Random();
	}

	/**
	 * CONSTRUCTOR: Create a new <code>ProcessGenerator</code> that randomly
	 * generates a new process within the given probability using the provided
	 * seed. Useful for debugging.
	 * 
	 * @param probability
	 *            the probability within which to generate a new Process.
	 * @param seed
	 *            the seed to use on the random.
	 */
	public ProcessGenerator(double probability, long seed) {
		this.probability = probability;
		rand = new Random(seed);
	}

	/**
	 * Generate a new <code>Process</code> with the specified values.
	 * 
	 * @param arrivalTime
	 *            the process's arrival time.
	 * @param processTime
	 *            processing time required for this Process to be complete.
	 * @param priorityLevel
	 *            the priority level of this process.
	 * @return a new Process with the specified values.
	 */
	public Process getNewProcess(int arrivalTime, int processTime, int priorityLevel) {
		return new Process(arrivalTime, rand.nextInt(processTime) + 1, rand.nextInt(priorityLevel) + 1);
	}

	/**
	 * Query the process generator to see if a new process has been generated.
	 * 
	 * @return <code>true</code> if a new process has been generated;
	 *         <code>false</code> otherwise.
	 */
	public boolean query() {
		return rand.nextDouble() <= probability;
	}
}