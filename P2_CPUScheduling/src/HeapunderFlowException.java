/**
 * The HeapunderFlowException is use when a index exceeds the root of a max heap.
 * @author Prince Kannah
 * @version 1.0 (Spring 16, CPU-Scheduling)
 * @see MaxHeap
 */
public class HeapunderFlowException extends RuntimeException {

    /**
     * Default constructor: creates a new exception.
     */
    public HeapunderFlowException(){super();}

    /**
     * Constructor: creates a new exception with the message
     * @param message the message to be shown when the exception is thrown.
     */
    public HeapunderFlowException(String message){
        super(message);
    }

}
