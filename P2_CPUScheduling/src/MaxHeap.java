import java.util.ArrayList;

/**
 * A heap is a data structure that is use to manage information. Heap in this context does not refer to garbage collection.
 * <br></br>The heap is an array object that can be viewed as a nearly complete binary tree. Each node of the tree is an element
 * of the array. Thus, given an index <i>i</i> the indices of its parent, left and right child can be computed as followed:<br></br>
 * <ol>
 *     PARENT(<i> i </i>): ⌊<i>i</i>/2⌋<br></br>
 *     LEFT(<i> i </i>): 2<i>i</i><br></br>
 *     RIGHT(<i> i </i>): 2<i>i</i> + 1
 * </ol>
 * The MaxHeap class is one implementation of the binary heap
 * (the other being MinHeap) of the heap data structure.
 * In a max-heap, all parent nodes <i>must</i> have values greater than or equal to their children. As such, the root of the
 * tree has the highest value.
 * <br></br>
 *
 * @param <T> the types of objects to be store at each node in this heap.
 * @author Prince Kannah
 * @version 1.0 (SPRING 16, P2 - CPU Scheduling)
 */

@SuppressWarnings ("unused")
public class MaxHeap <T extends Comparable<T>>{
    public static final int ROOT = 1;
    private int size;
    private ArrayList<T> heap;

    /**
     * CONSTRUCTOR: Creates a new MaxHeap with index starting at one (1).
     */
    public MaxHeap()
    {
        heap = new ArrayList<>();
        heap.add(null);
    }

    /**
     * "Heapify" starting at the specified index to maintain the max-heap property.<br></br>
     * <b><i>PRE-CONDITION:</i></b> Both the left and right subtree of the starting index <i>must</i> be max-heaps.
     * @param startingIndex the index of the node violating the max-heap property.
     */
    public void maxHeapify(int startingIndex){
        int largest;
        int leftChild = (startingIndex * 2);
        int rightChild = (startingIndex * 2) + 1;

        if(leftChild <= size && heap.get(leftChild).compareTo(heap.get(startingIndex)) == 1){
            largest = leftChild;
        }
        else
            largest = startingIndex;

        if(rightChild <= size && heap.get(rightChild).compareTo(heap.get(largest)) == 1){
            largest = rightChild;
        }

        if(largest != startingIndex){
            swap(startingIndex, largest);
            maxHeapify(largest);
        }
    }

    /**
     * Max-heapify the heap from the bottom up.
     */
    public void buildMaxHeap(){
        size = heap.size() - 1;
        for (int i =(heap.size()) / 2; i > 0 ; i--){
            maxHeapify(i);
        }
    }

    /**
     * Get the highest value node in the heap.
     * @return the highest value node in the heap.
     */
    public T maximum(){
      return heap.get(ROOT);
    }

    /**
     * Sort the heap using the object's natural ordering in place.
     */
    public void heapSort() {
        buildMaxHeap();
        int  heapSize = size;
        for (int i = heapSize; i > ROOT; i--){
            swap(ROOT,i);
            size--;
            maxHeapify(ROOT);
        }

        size = heap.size() - 1;
    }

    /**
     * Insert a node into the heap.
     * @param key the node to be inserted.
     */
    public void insert(T key){
        heap.add(ROOT,key);
        size = heap.size();
        buildMaxHeap();
    }

    /**
     * Get a node at the specified index.
     * @param index the index of the node.
     * @return the node at the specified index.
     */
    public T get(int index){
        if(index < ROOT){
            throw new HeapunderFlowException("Index must < ROOT (1)");
        }
        return heap.get(index);
    }

    /**
     * Removes (i.e. deletes) the node containing the highest value out of the heap.
     * @return the node with the biggest value.
     * @throws HeapunderFlowException if the total number of elements in the heap is less 1.
     */
    public T extractMax() {
        if(size < ROOT){
            throw new HeapunderFlowException("Size must < ROOT (1)");
        }

        T max = heap.remove(ROOT);
        size--;
        buildMaxHeap();
        return max;
    }

    /**
     * Update the value of the node at the specified index to the key.
     * @param index the index of the node.
     * @param key the new value for the node.
     * @throws KeyMismatchException if the value specified is smaller than the current value.
     */
    public void increaseKey(int index, T key){
        if(key.compareTo(heap.get(index)) == -1) {
            throw new KeyMismatchException("New value is smaller than current value");
        }

        heap.set(index,key);
        while (index  > ROOT && heap.get((index / 2 ) + 1).compareTo(heap.get(index)) == -1){
            swap(index,((index / 2) + 1));
            index = ((index / 2) + 1);
        }
        maxHeapify(ROOT);
    }

    /**
     * Exchange the objects at index p and j.
     * @param p the index of the first object
     * @param j the index of the second object.
     */
    private void swap(int p, int j){
        T temp = heap.get(p);
        heap.set(p, heap.get(j));
        heap.set(j,temp);
    }

    /**
     * Returns the MaxHeap object with all it's node.
     * @return the MaxHeap as an ArrayList<T>. <b>NOTE:</b> Indexing in a max-heap starts at one (1) <br></br>
     * so index zero (0) of the returned ArrayList contains a null node.
     */
    public ArrayList<T> getHeap(){
        return heap;
    }

    /**
     *Clear the heap of all data nodes.
     */
    public void clearHeap(){
        heap.clear();
        heap.add(null);
    }

    /**
     * Returns the size (i.e. the number of objects) in the heap.
     * @return the number of objects in the heap.
     */
    public int size(){
        return size;
    }

    @Override
    public String toString(){
        return heap.toString();
    }
}
