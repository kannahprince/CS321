/**
 * The <code>Process</code> class is use to represent a CPU process. Each object has a priority level, time required to process
 * <br></br>and arrival time. Processes are compared based on their priority levels. If those are equal then the
 * process with the earlier arrival time is "greater".
 * @author Prince Kannah
 * @version 1.0 (SPRING 16, P2 - CPU Scheduling)
 */
public class Process implements Comparable<Process>{
    private int priorityLevel;
    private int timeRemainingToFinish; //time remaining for this process to finish.
    private int arrivalTime;
    private int timeNotProcess;

    /**
     * CONSTRUCTOR: Create a new Process with the specified values.
     * @param arrivalTime the arrival time of this process.
     * @param requiredProcessTime the total time needed for this Process to complete.
     * @param priorityLevel the priority level for the process.
     */
    public Process(int arrivalTime, int requiredProcessTime, int priorityLevel){
        this.priorityLevel = priorityLevel;
        this.arrivalTime = arrivalTime;
        timeRemainingToFinish = requiredProcessTime;
        timeNotProcess = 0;
    }

    /**
     * Get the arrival time of this Process.
     * @return this process's arrival time
     */
    public int getArrivalTime(){
        return arrivalTime;
    }

    /**
     * Get the priority level of this Process.
     * @return this process's priority level.
     */
    public int getPriority(){
        return priorityLevel;
    }

    /**
     * Get the time remaining before this process is complete.
     * @return the time remaining before this process is complete.
     */
    public int getTimeRemaining(){
        return timeRemainingToFinish;
    }

    /**
     * Get the time not processed for this object.
     * @return the total number of time this Process has not received any work.
     */
    public int getTNP(){
        return timeNotProcess;
    }

    @Override
    public int compareTo(Process op){
        if(this.priorityLevel < op.priorityLevel){
            return - 1;
        }
        else if(this.priorityLevel > op.priorityLevel){
            return 1;
        }

        /*
        If priority levels are equal, look at arrival time
        and return the object with the earliest arrival time
         */
        else if(this.arrivalTime < op.arrivalTime){
            return 1;
        }
        return 0;
    }

    /**
     * Decrease the time remaining by one and reset the time not process.
     */
    public void reduceTimeRemaining(){
        timeRemainingToFinish--;
        resetTimeNotProcessed();
    }

    /**
     * Increment the time not process value by one (1).
     */
    public void incrementTNP(){
        timeNotProcess++;
    }

    /**
     * Increment this process's priority level by one (1).
     */
    public void incrementPriority(){
        priorityLevel++;
    }

    /**
     * Reset this process's time not process value to zero (0).
     */
    public void resetTimeNotProcessed(){
        timeNotProcess = 0;
    }

    /**
     * Returns <code>true</code> if this process is complete.
     * @return <code>true</code> if the process is complete; <code>false</code> otherwise.
     */
    public boolean finish(){
        return getTimeRemaining() == 0;
    }

    @Override
    public String toString(){
        return "[Priority level: " + getPriority() + ", Process time: " + getTimeRemaining()  +
                ", Arrival time: " + getArrivalTime() + "]";
    }
}
