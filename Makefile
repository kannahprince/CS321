.PHONY: build

build:
	@echo "P1: Caching"
	@cd P1_Cache && make
	@echo "P2: CPU Scheduling"
	@cd P2_CPUScheduling && make
	@echo "P3: Hash Table"
	@cd P3_Hashtable && make

Caching:
	@echo "Running P1: Caching"
	@cd P1_Cache && make Test

CPUScheduling:
	@echo "Running P2: CPU Scheduling"
	@cd P2_CPUScheduling && make CPUScheduling

HashTable:
	@echo "Running P3: Hash Table"
	@cd P3_Hashtable && make HashTest

clean:
	@echo "P1: Caching"
	@cd P1_Cache && make clean
	@echo "P2: CPU Scheduling"
	@cd P2_CPUScheduling && make clean
	@echo "P3: Hash Table"
	@cd P3_Hashtable && make clean
