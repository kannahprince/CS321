# CS321
This repository contains all my projects from CS321 (Data Structures) from Spring 2016. The class description can be found [here](http://registrar.boisestate.edu/undergraduate/course-catalog/cs/).
The specification for each project is listed below:

1. [Caching](https://drive.google.com/file/d/0B_E6vm7B7O5TUGJXNndRdFhCM1U/view?usp=sharing)
2. [CPU Scheduling](https://drive.google.com/file/d/0B_E6vm7B7O5TamtuYllzWEIxOXM/view?usp=sharing)
3. [Hash Table](https://drive.google.com/file/d/0B_E6vm7B7O5TRm1Ra0g3TXV1eW8/view?usp=sharing)

## Project Setup
There is a top-level [Makefile](https://www.gnu.org/software/make/) that can compile and run all projects. To compile and run a specific project issue the command `make <projectname>`. No space for multi-word project. For example: ` make Caching` will compile and run project one. 

Each project is also setup with a makefile. See each project's __README__ for more info on providing project specific command line arguments.
